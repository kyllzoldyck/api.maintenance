<?php

use Illuminate\Database\Seeder;

class AccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accesses')->insert([
            'id_print_menu' => 1,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        DB::table('accesses')->insert([
            'id_print_menu' => 2,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1            
        ]);

        DB::table('accesses')->insert([
            'id_print_menu' => 3,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        DB::table('accesses')->insert([
            'id_print_menu' => 4,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        DB::table('accesses')->insert([
            'id_print_menu' => 5,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        DB::table('accesses')->insert([
            'id_print_menu' => 6,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        DB::table('accesses')->insert([
            'id_print_menu' => 7,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        DB::table('accesses')->insert([
            'id_print_menu' => 8,
            'id_entity' => 1,
            'id_rol' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 0
        ]);

        // DB::table('accesses')->insert([
        //     'id_print_menu' => 9,
        //     'id_entity' => 1,
        //     'id_rol' => 1,
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'updated_at' => date('Y-m-d H:i:s'),
        //     'state' => 1
        // ]);

        // DB::table('accesses')->insert([
        //     'id_print_menu' => 10,
        //     'id_entity' => 1,
        //     'id_rol' => 1,
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'updated_at' => date('Y-m-d H:i:s'),
        //     'state' => 1
        // ]);
    }
}
