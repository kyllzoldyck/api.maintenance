<?php

use Illuminate\Database\Seeder;

class PrintMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # 1
        DB::table('print_menus')->insert([
            'id_menu' => 0,
            'id_sub_menu' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 2
        DB::table('print_menus')->insert([
            'id_menu' => 1,
            'id_sub_menu' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);
        
        # 3
        DB::table('print_menus')->insert([
            'id_menu' => 2,
            'id_sub_menu' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 4
        DB::table('print_menus')->insert([
            'id_menu' => 0,
            'id_sub_menu' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 5
        DB::table('print_menus')->insert([
            'id_menu' => 4,
            'id_sub_menu' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 6
        DB::table('print_menus')->insert([
            'id_menu' => 0,
            'id_sub_menu' => 6,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 7
        DB::table('print_menus')->insert([
            'id_menu' => 6,
            'id_sub_menu' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 8
        DB::table('print_menus')->insert([
            'id_menu' => 6,
            'id_sub_menu' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 9
        DB::table('print_menus')->insert([
            'id_menu' => 0,
            'id_sub_menu' => 9,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

        # 10
        DB::table('print_menus')->insert([
            'id_menu' => 9,
            'id_sub_menu' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1
        ]);

    }
}
