<?php

use Faker\Generator as Faker;
use \Freshwork\ChileanBundle\Rut;

$factory->define(App\Entity::class, function (Faker $faker) {

    $random_number = rand(1000000, 25000000);
    $rut = new Rut($random_number);

    return [
        'name' => $faker->name,
        'state' => 1,
        'razon_social' => $faker->company,
        'rut' => $rut->fix()->format(),
        'url_img' => $faker->url,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
    ];
});
