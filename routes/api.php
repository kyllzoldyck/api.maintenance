<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'menus'], function () {
    Route::get('/', 'MenuController@index')->name('menus.index');
    Route::get('/{id}', 'MenuController@show')->name('menus.show');
    Route::post('/', 'MenuController@store')->name('menus.store');
    Route::put('/{id}', 'MenuController@update')->name('menus.update');    
    Route::delete('/{id}', 'MenuController@destroy')->name('menus.destroy');
});

Route::group(['prefix' => 'roles'], function () {
    Route::get('/', 'RolController@index')->name('roles.index');
    Route::get('/{id}', 'RolController@show')->name('roles.show');
    Route::post('/', 'RolController@store')->name('roles.store');
    Route::put('/{id}', 'RolController@update')->name('roles.update');    
    Route::delete('/{id}', 'RolController@destroy')->name('roles.destroy');
});

Route::group(['prefix' => 'entities'], function () {
    Route::get('/', 'EntityController@index')->name('entities.index');
    Route::get('/{id}', 'EntityController@show')->name('entities.show');
    Route::post('/', 'EntityController@store')->name('entities.store');
    Route::put('/{id}', 'EntityController@update')->name('entities.update');    
    Route::delete('/{id}', 'EntityController@destroy')->name('entities.destroy');
});

Route::group(['prefix' => 'print_menus'], function () {
    Route::get('/', 'PrintMenuController@index')->name('print_menus.index');
    Route::get('/{id}', 'PrintMenuController@show')->name('print_menus.show');
    Route::post('/', 'PrintMenuController@store')->name('print_menus.store');
    Route::put('/{id}', 'PrintMenuController@update')->name('print_menus.update');
    Route::delete('/{id}', 'PrintMenuController@destroy')->name('print_menus.destroy');
});

Route::group(['prefix' => 'accesses'], function () {
    Route::get('/roles/entities/', 'AccessController@getAccessesByRolAndEntity')->name('accesses.getAccessesByRolAndEntity');
    Route::post('/', 'AccessController@store')->name('accesses.store');
    Route::put('/{id}', 'AccessController@update')->name('accesses.update');
    Route::delete('/{id}', 'AccessController@destroy')->name('accesses.destroy');
});