<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';

    protected $fillable = [
        'id',
        'name',
        'icon',
        'uri'
    ];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $connection = '';

    function printMenus()
    {
        return $this->hasMany('App\PrintMenu', 'id_menu', 'id');
    }

    /**
     * Get Menu by Id
     */
    function getMenu($id)
    {
        return Menu::find($id);
    }

    /**
     * Get Menu By Id and State
     *
     * @param $id
     */
    function getMenuWithState($id, $state)
    {
        return Menu::where('state', $state)->where('id', $id)->first();
    }

    /**
     * Get All Menu by State
     */
    function getMenus($state)
    {
        return Menu::where('state', $state)->get();
    }
}
