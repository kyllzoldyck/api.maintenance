<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    protected $table = 'entities';

    protected $fillable = [
        'name',
        'state',
        'razon_social',
        'rut',
        'url_img'
    ];

    public $timestamps = true;

    protected $primaryKey = 'id';

    protected $connection = '';

    function accesses()
    {
        return $this->hasMany('App\Access', 'id_entity', 'id');
    }

    /**
     * Get Entity By Id And State
     */
    function getEntityWithState($id, $state)
    {
        return Entity::where('state', $state)->where('id', $id)->first();
    }

    /**
     * Get Entity By Id 
     */
    function getEntity($id)
    {
        return Entity::find($id);
    }

    /**
     * Get Entities By State
     */
    function getEntities($state)
    {
        return Entity::where('state', $state)->get();
    }
}
