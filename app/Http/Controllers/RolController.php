<?php

namespace App\Http\Controllers;

use App\Rol;
use App\Http\Requests\RolRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Hi, getting roles');
        
        try
        {
            $rolObject = new Rol();
            $roles = $rolObject->getRols(1) ;
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Roles obtained');
        return response()->json($roles, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolRequest $request)
    {
        $rol = new Rol();

        Log::info('Hi, creating rol...');

        $rol->name = strtolower(trim($request->input('name')));
        $rol->created_at = date('Y-m-d H:i:s');
        $rol->updated_at = date('Y-m-d H:i:s');
        $rol->state = $request->input('state');

        try
        {
            Log::info('Saving rol as ' . $rol->name);
            $rol->save();
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['created', false], 202);
        }

        Log::info('Rol created as ' . $rol->name);
        return response()->json(['created' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info('Hi, getting single rol.');

        if (!is_numeric($id))
        {
            Log::warning('El id del rol no es numérico.');
            return response()->json(['get' => false], 422);
        }

        try
        {
            $rolObject = new Rol();
            $rol = $rolObject->getRolWithState($id, 1);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Menu obtained ' . $rol->id);
        return response()->json($rol, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function edit(Rol $rol)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(RolRequest $request, $id)
    {
        Log::info('Hi, updating rol');

        if (!is_numeric($id)) 
        {
            Log::warning('El id del rol no es numérico.');
            return response()->json(['updated' => false, 'error' => 'El id del rol no es numérico.'], 422);
        } 
        
        $rolObject = new Rol();
        $rol = $rolObject->getRol($id);

        if (count($rol) == 0) 
        {
            Log::warning('No se ha encontrado el rol.');
            return response()->json(['updated' => false, 'error' => 'No se ha encontrado el rol.'], 204);
        }

        $rol->name = strtolower(trim($request->input('name')));
        $rol->updated_at = date('Y-m-d H:i:s');
        $rol->state = $request->input('state');

        try 
        {
            Log::info('Updating rol ' . $id);
            $rol->save();
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['updated' => false], 202);
        }

        Log::info('Rol updated');
        return response()->json(['updated' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info('Hi, deleting rol');

        if (!is_numeric($id)) 
        {
            Log::warning('El id del rol no es numérico.');
            return response()->json(['deleted' => false, 'error' => 'El id del rol no es numérico.'], 422);
        } 
            
        $rolObject = new Rol();
        $rol = $rolObject->getRol($id);

        if (count($rol) == 0) 
        {
            Log::warning('No se ha encontrado el rol.');
            return response()->json(['deleted' => false, 'error' => 'No se ha encontrado el rol.'], 204);
        }

        try
        {
            $rol->state = 0;
            $rol->save();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['deleted' => false], 204);
        }

        Log::info('Rol ' . $rol->id . ' deleted.');
        return response()->json(['deleted' => true], 200);
    }
}
