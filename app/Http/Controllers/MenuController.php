<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\MenuRequest;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Hi, getting menus');
        
        try
        {
            $menuObject = new Menu();
            $menus = $menuObject->getMenus(1);
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Menus obtained');
        return response()->json($menus, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {        
        try
        {
            $menu = new Menu();
    
            Log::info('Hi, creating menu...');
    
            $menu->name = strtolower(trim($request->input('name')));
            $menu->icon = trim($request->input('icon'));
            $menu->uri = trim($request->input('uri'));
            $menu->created_at = date('Y-m-d H:i:s');
            $menu->updated_at = date('Y-m-d H:i:s');
            $menu->state = $request->input('state');
            
            Log::info('Saving menu as ' . $menu->name);
            $menu->save();
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['created', false], 202);
        }

        Log::info('Menu created as ' . $menu->name);
        return response()->json(['created' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info('Hi, getting single menu.');

        if (!is_numeric($id))
        {
            Log::warning('El id del menu no es numérico.');
            return response()->json(['get' => false], 422);
        }

        try
        {
            $menuObject = new Menu();
            $menu = $menuObject->getMenuWithState($id, 1);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Menu obtained ' . $menu->id);
        return response()->json($menu, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        Log::info('Hi, updating menu');

        if (!is_numeric($id)) 
        {
            Log::warning('El id del menu no es numérico.');
            return response()->json(['updated' => false, 'error' => 'El id del menu no es numérico.'], 422);
        } 
        
        $menuObject = new Menu();
        $menu = $menuObject->getMenu($id);

        if (count($menu) == 0) 
        {
            Log::warning('No se ha encontrado el menu.');
            return response()->json(['updated' => false, 'error' => 'No se ha encontrado el menu.'], 204);
        }

        $menu->name = strtolower(trim($request->input('name')));
        $menu->icon = trim($request->input('icon'));
        $menu->uri = trim($request->input('uri'));
        $menu->updated_at = date('Y-m-d H:i:s');
        $menu->state = $request->input('state');

        try 
        {
            Log::info('Updating menu ' . $id);
            $menu->save();
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['updated' => false], 202);
        }

        Log::info('Menu updated');
        return response()->json(['updated' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info('Hi, deleting menu');

        if (!is_numeric($id)) 
        {
            Log::warning('El id del menu no es numérico.');
            return response()->json(['deleted' => false, 'error' => 'El id del menu no es numérico.'], 422);
        } 
            
        $menuObject = new Menu();
        $menu = $menuObject->getMenu($id);

        if (count($menu) == 0) 
        {
            Log::warning('No se ha encontrado el menu.');
            return response()->json(['deleted' => false, 'error' => 'No se ha encontrado el menu.'], 204);
        }

        try
        {
            $menu->state = 0;
            $menu->save();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['deleted' => false], 204);
        }

        Log::info('Menu ' . $menu->id . ' deleted.');
        return response()->json(['deleted' => true], 200);
    }
}
