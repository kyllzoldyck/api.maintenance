<?php

namespace App\Http\Controllers;

use App\Access;
use App\PrintMenu;
use App\Rol;
use App\Entity;
use App\Http\Requests\AccessRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Display a listing of Accesses from a Rol and Entity
     * 
     * @param \Illuminate\Http\Request $request
     */
    public function getAccessesByRolAndEntity(Request $request) 
    {
        Log::info('Hi, getting accesses');

        try
        {
            $rol = trim($request->input('id_rol'));
            $entity = trim($request->input('id_entity'));

            if (!is_numeric($rol) || !is_numeric($entity)) 
            {
                Log::warning('Rol or Entity is not a number');
                return response()->json(['get' => false, 'error' => 'No se han podido obtener los accesos.'], 422);
            }

            Log::info('Getting Rol by Id');
            $rolObject = new Rol();

            if (count($rolObject->getRol($rol)) == 0) 
            {
                Log::warning('Rol not exists');
                return response()->json(['get' => false, 'error' => 'El rol no existe.'], 204);
            }

            Log::info('Getting Entity by Id');
            $entityObject = new Entity();

            if (count($entityObject->getEntity($entity)) == 0) 
            {
                Log::warning('Entity not exists');
                return response()->json(['get' => false, 'error' => 'El rol no existe.'], 204);
            }

            Log::info('Getting Accessess by Rol And Entity');
            $accessObject = new Access();

            $accesses = $accessObject->getAccessesByRolAndEntity($rol, $entity, 1);

            if (count($accesses) == 0)
            {
                Log::info('There is not information.');
                return response()->json(['get' => false, 'error' => 'El Acceso no existe o se encuentra deshabilitado.'], 204);
            }    

            $printMenuObject = new PrintMenu();

            Log::info('Creating Print Menu');
            $printMenu = $printMenuObject->printMenu($accesses);

            if (count($printMenu) == 0)
            {
                Log::warning('The menu could not be created');
                return response()->json(['get' => false, 'error' => 'El menu no pudo ser creado.'], 204);
            }

        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Accesses obtained');
        return response()->json($printMenu, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\AccessRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccessRequest $request)
    {
        try
        {
            Log::info('Hi, creating access');

            $idPrintMenu = trim($request->input('id_print_menu'));
            $idEntity = trim($request->input('id_entity'));
            $idRol = trim($request->input('id_rol'));

            $printMenuObject = new PrintMenu();
            
            if (count($printMenuObject->getPrintMenu($idPrintMenu)) == 0)
            {
                Log::info('Print Menu not exists or is disabled');
                return response()->json(['created' => false, 'error' => 'El menu no existe o se encuentra deshabilitado.'], 204);
            }

            $entityObject = new Entity();

            if (count($entityObject->getEntity($idEntity)) == 0)
            {
                Log::info('Entity not exists or is disabled');
                return response()->json(['created' => false, 'error' => 'La entidad no existe o se encuentra deshabilitada.'], 204);
            }

            $rolObject = new Rol();

            if (count($rolObject->getRol($idRol)) == 0)
            {
                Log::info('Rol not exists or is disabled');
                return response()->json(['created' => false, 'error' => 'El rol no existe o se encuentra deshabilitado.'], 204);
            }

            $accessObject = new Access();

            if ($accessObject->verifyAccess($idPrintMenu, $idEntity, $idRol))
            {
                # Access exists
                Log::info('Access Exists');
                return response()->json(['created' => false, 'error' => 'El Acceso ya existe.'], 202);
            }

            $accessObject->id_print_menu = $idPrintMenu;
            $accessObject->id_entity = $idEntity;
            $accessObject->id_rol = $idRol;
            $accessObject->state = 1;
            $accessObject->created_at = date('Y-m-d H:i:s');
            $accessObject->updated_at = date('Y-m-d H:i:s');

            Log::info('Saving access');
            $accessObject->save();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['created' => false], 202);
        }

        Log::info('Access created');
        return response()->json(['created' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function show(Access $access)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function edit(Access $access)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\AccessRequest  $request
     * @param  \App\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function update(AccessRequest $request, $id)
    {
        try
        {
            Log::info('Hi, updating access');

            $idPrintMenu = trim($request->input('id_print_menu'));
            $idEntity = trim($request->input('id_entity'));
            $idRol = trim($request->input('id_rol'));

            $accessObject = new Access();

            if (count($accessObject->getAccessWithState($id, 1)) == 0)
            {
                Log::info('Access not exists or is disabled');
                return response()->json(['updated' => false, 'error' => 'El Acceso no existe o se encuentra deshabilitado.'], 204);
            }

            $printMenuObject = new PrintMenu();
            
            if (count($printMenuObject->getPrintMenu($idPrintMenu)) == 0)
            {
                Log::info('Print Menu not exists or is disabled');
                return response()->json(['updated' => false, 'error' => 'El menu no existe o se encuentra deshabilitado.'], 204);
            }

            $entityObject = new Entity();

            if (count($entityObject->getEntity($idEntity)) == 0)
            {
                Log::info('Entity not exists or is disabled');
                return response()->json(['updated' => false, 'error' => 'La entidad no existe o se encuentra deshabilitada.'], 204);
            }

            $rolObject = new Rol();

            if (count($rolObject->getRol($idRol)) == 0)
            {
                Log::info('Rol not exists or is disabled');
                return response()->json(['updated' => false, 'error' => 'El Rol no existe o se encuentra deshabilitado.'], 204);
            }

            if ($accessObject->verifyAccess($idPrintMenu, $idEntity, $idRol))
            {
                # Access exists
                Log::info('Access Exists');
                return response()->json(['updated' => false, 'error' => 'El Acceso ya existe.'], 202);
            }

            $accessObject = $accessObject->getAccess($id);
            $accessObject->id_print_menu = $idPrintMenu;
            $accessObject->id_entity = $idEntity;
            $accessObject->id_rol = $idRol;
            $accessObject->updated_at = date('Y-m-d H:i:s');

            Log::info('Updating access');
            $accessObject->save();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['updated' => false], 202);
        }

        Log::info('Access updated');
        return response()->json(['updated' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            Log::info('Hi, deleting access');

            if (!is_numeric($id))
            {
                Log::info('Access is not a number');
                return response()->json(['delete' => false, 'error' => 'El id no es un número.'], 422);
            }

            $accessObject = new Access();
            $access = $accessObject->getAccess($id);

            if (count($access) == 0)
            {
                Log::info('Access not exists');
                return response()->json(['delete' => false, 'error' => 'El acceso no existe.'], 204);
            }

            $access->state = 0;

            Log::info('Deleting access');
            $access->save();
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['delete' => false], 202);
        }

        Log::info('Access with id: ' . $id . ' deleted');
        return response()->json(['delete' => true], 200);
    }
}
