<?php

namespace App\Http\Controllers;

use App\PrintMenu;
use App\Menu;
use App\Http\Requests\PrintMenuRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class PrintMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Hi, getting print menus');
        
        try
        {   
            $printMenuObject = new PrintMenu();
            $printMenus = $printMenuObject->getPrintMenus(1);
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Print Menus obtained');
        return response()->json($printMenus, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PrintMenuRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrintMenuRequest $request)
    {
        Log::info('Hi, creating PrintMenu');

        try
        {
            $idMenu = trim($request->input('id_menu'));
            $idSubMenu = trim($request->input('id_sub_menu'));

            $menuObject = new Menu();

            if ($idMenu > 0 && count($menuObject->getMenuWithState($idMenu, 1)) == 0)
            {
                Log::info('Parent Menu not exists or is disabled');
                return response()->json(['created' => false], 204);
            }

            if (count($menuObject->getMenuWithState($idSubMenu, 1)) == 0)
            {
                Log::info('Child Menu not exists or is disabled');
                return response()->json(['created' => false], 204);
            }

            if ($idSubMenu == $idMenu) 
            {
                Log::info('Child Menu is equal to Parent Id');
                return response()->json(['created' => false], 422);
            }

            $printMenu = new PrintMenu();

            $printMenu->id_menu = $idMenu;
            $printMenu->id_sub_menu = $idSubMenu;
            $printMenu->created_at = date('Y-m-d H:i:s');
            $printMenu->updated_at = date('Y-m-d H:i:s');
            $printMenu->state = 1;

            Log::info('Saving print menu');
            $printMenu->save();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['created' => false], 202);
        }
        
        Log::info('Print menu created');
        return response()->json(['created' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            Log::info('Hi, getting print menu');

            if (!is_numeric($id))
            {
                Log::info('Id print menu is not a number');
                return response()->json(['get' => false], 422);
            }

            $printMenuObject = new PrintMenu();
            $printMenu = $printMenuObject->getPrintMenuWithState($id, 1);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Print Menu Obtained');
        return response()->json($printMenu);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PrintMenu  $printMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(PrintMenu $printMenu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PrintMenu  $printMenu
     * @return \Illuminate\Http\Response
     */
    public function update(PrintMenuRequest $request, $id)
    {
        try
        {
            Log::info('Hi, updating print menu');

            if (!is_numeric($id))
            {
                Log::warning('Id print menu is not a number');
                return response()->json(['updated' => false], 422);
            }

            $idMenu = trim($request->input('id_menu'));
            $idSubMenu = trim($request->input('id_sub_menu'));
            $state = trim($request->input('state'));

            if ($idSubMenu == $idMenu) 
            {
                Log::info('Child menu is equal to parent');
                return response()->json(['updated' => false], 422);
            }

            $printMenuObject = new PrintMenu();
            $printMenu = $printMenuObject->getPrintMenu($id);

            if (count($printMenu) == 0) 
            {
                Log::info('Print menu not exists');
                return response()->json(['updated' => false], 204);
            }

            $printMenu->id_menu = $idMenu;
            $printMenu->id_sub_menu = $idSubMenu;
            $printMenu->state = $state;
            $printMenu->updated_at = date('Y-m-d H:i:s');

            Log::info('Updating print menu');
            $printMenu->save();

        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['updated' => false], 202);
        }

        Log::info('Print menu updated. ID: ' . $printMenu->id);
        return response()->json(['updated' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            Log::info('Hi, deleting print menu');

            if (!is_numeric($id))
            {
                Log::warning('Id print menu is not a number');
                return response()->json(['deleted' => false], 422);
            }

            $printMenuObject = new PrintMenu();
            
            $printMenu = $printMenuObject->getPrintMenu($id);
            $printMenu->state = 0;

            Log::info('Disabling print menu');
            $printMenu->save();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['deleted' => false], 202);
        }

        Log::info('Print menu deleted');
        return response()->json(['deleted' => true], 200);
    }
}
