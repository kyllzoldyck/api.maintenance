<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:100|min:5',
            'icon' => 'max:150',
            'uri' => 'max:100',
            'state' => 'required|numeric|boolean'
        ];

        switch ($this->method())
        {
            case 'DELETE':
                $rules = [
                    'id' => 'required|numeric'
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => 'El id del menu es obligatorio.',
            'id.numeric' => 'El id del menu debe ser numérico.',

            'name.required' => 'El nombre del menu es obligatorio.',
            'name.max:100' => 'El nombre del menu debe tener un máximo de 100 caracteres.',
            'name.min:5' => 'El nombre del menu debe tener un minimo de 5 caracteres.',

            'icon.max:150' => 'El icono debe tener un máximo de 100 caracteres.',

            'uri.max:100' => 'El URI debe tener un máximo de 100 caracteres.',

            'state.required' => 'El estado del menu es obligatorio.',
            'state.numeric' => 'El estado es de tipo numérico.',
            'state.boolean' => 'El estado debe ser 1 o 0.'
        ];
    }
}
