<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_print_menu' => 'required|numeric',
            'id_entity' => 'required|numeric',
            'id_rol' => 'required|numeric'
        ];

        switch ($this->method())
        {
            case 'PUT':
                array_push($rules, ['state' => 'required|numeric|boolean']);
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'id_print_menu.required' => 'El id del menu es obligatorio.',
            'id_print_menu.numeric' => 'El id del menu debe ser numérico.',
            
            'id_entity.required' => 'El id de la entidad es obligatorio.',
            'id_entity.numeric' => 'El id de la entidad debe ser numérico.',

            'id_rol.required' => 'El id del rol es obligatorio.',
            'id_rol.numeric' => 'El id del rol debe ser numérico.',

            'state.required' => 'El estado es obligatorio.',
            'state.numeric' => 'El estado es de tipo numérico.',
            'state.boolean' => 'El estado debe ser 1 o 0.'
        ];
    }
}
