<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Access extends Model
{
    protected $table = 'accesses';

    protected $fillable = [
        'id',
        'name',
        'state'
    ];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $connection = '';

    function rol()
    {
        return $this->belongsTo('App\Rol', 'id_rol');
    }

    function printMenu()
    {
        return $this->belongsTo('App\PrintMenu', 'id_print_menu');
    }

    function entity()
    {
        return $this->belongsTo('App\Entity', 'id_entity');
    }

    /**
     * Get Access By Id And State
     */
    function getAccessWithState($id, $state)
    {
        return Access::where('state', $state)->where('id', $id)->first();
    }

    /**
     * Get Access By Id 
     */
    function getAccess($id)
    {
        return Access::find($id);
    }

    /**
     * Get Accesses By State
     */
    function getAccesses($state)
    {
        return Access::where('state', $state)->get();
    }

    /**
     * Verify if exists Access
     * 
     * @return true if exists access
     */
    function verifyAccess($idPrintMenu, $idEntity, $idRol)
    {
        return count(Access::where('id_print_menu', $idPrintMenu)->where('id_entity', $idEntity)->where('id_rol', $idRol)->first()) > 0;
    }

    function getAccessesByRolAndEntity($rol, $entity, $state) 
    {
        return DB::select(
            "SELECT
                AC.id,
                AC.created_at,
                AC.updated_at,
                AC.state AS 'state_access',
                ME.name AS 'name_menu',
                ME.icon,
                ME.uri,
                ME.state AS 'state_menu',
                PM.id_menu AS 'id_menu',
                PM.id_sub_menu AS 'id_sub_menu',
                EN.name AS 'name_entity'
            FROM 
                accesses AC,
                print_menus PM,
                entities EN,
                rols RL,
                menus ME
            WHERE 
                AC.id_print_menu = PM.id AND
                AC.id_entity = EN.id AND
                AC.id_rol = RL.id AND
                PM.id_sub_menu = ME.id AND
                AC.state = ? AND
                RL.id = ? AND
                EN.id = ?
            ORDER BY 
                ME.name
            DESC"
        , 
        [
            1, 
            $rol, 
            $entity
        ]);
    }
}
