<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrintMenu extends Model
{
    protected $table = 'print_menus';

    protected $fillable = [
        'id_menu',
        'id_sub_menu',
        'created_at',
        'updated_at',
        'state'
    ];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $connection = '';

    function accesses() 
    {
        return $this->hasMany('App\Access', 'id_print_menu', 'id');
    }

    function menu() 
    {
        return $this->belongsTo('App\Menu', 'id_menu');
    }

    /**
     * Get Print Menus by State
     */
    function getPrintMenus($state) 
    {
        return PrintMenu::where('state', $state)->get();
    }

    /**
     * Get Print Menu By Id And State
     */
    function getPrintMenuWithState($id, $state)
    {
        return PrintMenu::where('state', $state)->where('id', $id)->get();
    }

    /**
     * Get Print Menu By Id
     */
    function getPrintMenu($id)
    {
        return PrintMenu::find($id);
    }

    /**
     * Print Menu
     */
    function printMenu($accesses, $id_parent = 0)
    {
        $accessesArray = (array) $accesses;
        $printMenu = array();
    
        foreach ($accessesArray as $access) 
        {
            $accessArray = (array) $access;

            if ($accessArray['id_menu'] == $id_parent)
            {
                $children = $this->printMenu($accesses, $accessArray['id_sub_menu']);

                if ($children)
                {
                    $accessArray['children'] = $children;
                }
                $printMenu[] = $accessArray;
            }
        }

        return $printMenu;
    }
}