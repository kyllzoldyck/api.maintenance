<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RolTest extends TestCase
{
    use WithFaker;
    
    public function testShouldInsertRol() 
    {
        $response = $this->json(
            'POST', 
            '/api/roles', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 1
            ]
        );

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true
            ]);
    }

        
    public function testShouldNotInsertRolForBadParameters() 
    {
        $response = $this->json(
            'POST', 
            '/api/roles', 
            [
                'name' => '',
                'state' => 100
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotInsertRolForBadMethod()
    {
        $response = $this->json(
            'PUT', 
            '/api/roles', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 1
            ]
        );

        $response->assertStatus(405);
    }

    public function testShouldUpdateRol() 
    {
        $response = $this->json(
            'PUT', 
            '/api/roles/1', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 1
            ]
        );

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true
            ]);
    }

    public function testShouldNotUpdateRolForBadParameters()
    {
        $response = $this->json(
            'PUT', 
            '/api/roles/1', 
            [
                'name' => '',
                'state' => 0
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotUpdateRolForStringId()
    {
        $response = $this->json(
            'PUT', 
            '/api/roles/string_id', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 1
            ]
        );

        $response
            ->assertStatus(422)
            ->assertJson([
                'updated' => false
            ]);
    }

    public function testShouldNotUpdateRolForRolNotExists()
    {
        $response = $this->json(
            'PUT', 
            '/api/roles/1231231412', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 1
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldDeleteRol()
    {
        $response = $this->json(
            'DELETE', 
            '/api/roles/1', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 0
            ]
        );

        $response
            ->assertStatus(200)
            ->assertJson(['deleted' => true]);
    }

    public function testShouldNotDeleteRolForStringId()
    {
        $response = $this->json(
            'DELETE', 
            '/api/roles/string_id', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 0
            ]
        );

        $response
            ->assertStatus(422)
            ->assertJson(['deleted' => false]);
    }

    public function testShouldNotDeleteRolForRolNotExists()
    {
        $response = $this->json(
            'DELETE', 
            '/api/roles/112421412', 
            [
                'name' => 'Rol ' . $this->faker->name,
                'state' => 0
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldGetRoles()
    {
        $response = $this->get('/api/roles/');

        $response->assertStatus(200);
    }

    public function testShouldGetSingleRol()
    {
        $response = $this->get('/api/roles/2');

        $response->assertStatus(200);
    }

    public function testShouldNotGetSingleRolForStringId()
    {
        $response = $this->get('/api/roles/string_id');

        $response->assertStatus(422);
    }
}
