<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccessTest extends TestCase
{
    public function testShouldNotGetAccessesByRolOrEntityIsNotANumber()
    {
        $response = $this->get('/api/accesses/roles/entities/?id_rol=string_id&id_entity=string_id');

        $response->assertStatus(422);
    }

    public function testShouldNotGetAccessesByRolNotExists()
    {
        $response = $this->get('/api/accesses/roles/entities/?id_rol=1241241&id_entity=1');

        $response->assertStatus(204);
    }

    public function testShouldNotGetAccessesByEntityNotExists()
    {
        $response = $this->get('/api/accesses/roles/entities/?id_rol=1&id_entity=112312');

        $response->assertStatus(204);
    }

    public function testShouldNotGetAccessesByAccessesNotExists()
    {
        $response = $this->get('/api/accesses/roles/entities/?id_rol=2&id_entity=2');

        $response->assertStatus(204);
    }

    public function testShouldNotGetAccessesByRolIsStringId()
    {
        $response = $this->get('/api/accesses/roles/entities/?id_rol=string_id&id_entity=1');

        $response->assertStatus(422);
    }

    public function testShouldNotGetAccessesByEntityIsStringId()
    {
        $response = $this->get('/api/accesses/roles/entities/?id_rol=1&id_entity=string_id');

        $response->assertStatus(422);
    }

    public function testShouldGetAccessesByRolAndEntity()
    {
        $response = $this->get('/api/accesses/roles/entities/?id_rol=1&id_entity=1');

        $response->assertStatus(200);
    }

    public function testShouldNotInsertAccessByPrintMenuNotExists()
    {
        $response = $this->json(
            'POST',
            '/api/accesses',
            [
                'id_print_menu' => 1124124,
                'id_entity' => 1,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldNotInsertAccessByEntityNotExists()
    {
        $response = $this->json(
            'POST',
            '/api/accesses',
            [
                'id_print_menu' => 1,
                'id_entity' => 1123123,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldNotInsertAccessByRolNotExists()
    {
        $response = $this->json(
            'POST',
            '/api/accesses',
            [
                'id_print_menu' => 1,
                'id_entity' => 1,
                'id_rol' => 112312312
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldNotInsertAccessByAccessExists()
    {
        $response = $this->json(
            'POST',
            '/api/accesses',
            [
                'id_print_menu' => 1,
                'id_entity' => 1,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(202);
    }

    public function testShouldInsertAccess()
    {
        $response = $this->json(
            'POST',
            '/api/accesses',
            [
                'id_print_menu' => 9,
                'id_entity' => 1,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(201);
    }

    public function testShouldNotUpdateAccessByAccessIsDisabledOrNotExists()
    {
        $response = $this->json(
            'PUT',
            '/api/accesses/8',
            [
                'id_print_menu' => 1,
                'id_entity' => 1,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldNotUpdateAccessByPrintMenuNotExists()
    {
        $response = $this->json(
            'PUT',
            '/api/accesses/1',
            [
                'id_print_menu' => 1124124,
                'id_entity' => 1,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldNotUpdateAccessByEntityNotExists()
    {
        $response = $this->json(
            'PUT',
            '/api/accesses/1',
            [
                'id_print_menu' => 1,
                'id_entity' => 1123123,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldNotUpdateAccessByRolNotExists()
    {
        $response = $this->json(
            'PUT',
            '/api/accesses/1',
            [
                'id_print_menu' => 1,
                'id_entity' => 1,
                'id_rol' => 112312312
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldNotUpdateAccessByAccessExists()
    {
        $response = $this->json(
            'PUT',
            '/api/accesses/1',
            [
                'id_print_menu' => 1,
                'id_entity' => 1,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(202);
    }

    public function testShouldUpdateAccess()
    {
        $response = $this->json(
            'PUT',
            '/api/accesses/7',
            [
                'id_print_menu' => 10,
                'id_entity' => 1,
                'id_rol' => 1
            ]
        );

        $response->assertStatus(200);
    }

    public function testShouldNotDeleteAccessByStringId()
    {
        $response = $this->delete('/api/accesses/string_id');

        $response->assertStatus(422);
    } 

    public function testShouldNotDeleteAccessByAccessNotExists()
    {
        $response = $this->delete('/api/accesses/1123123');

        $response->assertStatus(204);
    } 
    public function testShouldDeleteAccess()
    {
        $response = $this->delete('/api/accesses/1');

        $response->assertStatus(200);
    } 
}
