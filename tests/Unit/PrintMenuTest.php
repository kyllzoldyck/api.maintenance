<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PrintMenuTest extends TestCase
{
    public function testShouldGetPrintMenus()
    {
        $response = $this->get('/api/print_menus');

        $response->assertStatus(200);
    }

    public function testShouldGetPrintMenu()
    {
        $response = $this->get('/api/print_menus/1');

        $response->assertStatus(200);
    }

    public function testShouldNotGetPrintMenuForStringId()
    {
        $response = $this->get('/api/print_menus/string_id');

        $response->assertStatus(422);
    }

    public function testShouldNotInsertPrintMenuForBadParameters()
    {
        $response = $this->json(
            'POST',
            '/api/print_menus',
            [
                'id_menu' => '123',
                'id_sub_menu' => ''
            ]
        );
        
        $response->assertStatus(422);
    }

    public function testShouldNotInsertPrintMenuForParentMenuNotExists()
    {
        $response = $this->json(
            'POST',
            '/api/print_menus',
            [
                'id_menu' => 41221312,
                'id_sub_menu' => 9
            ]
        );
        
        $response->assertStatus(204);
    }

    public function testShouldNotInsertPrintMenuForChildMenuNotExists()
    {
        $response = $this->json(
            'POST',
            '/api/print_menus',
            [
                'id_menu' => 0,
                'id_sub_menu' => 12312314
            ]
        );
        
        $response->assertStatus(204);
    }

    public function testShouldNotInsertPrintMenuForSubMenuIsEqualToParent()
    {
        $response = $this->json(
            'POST',
            '/api/print_menus',
            [
                'id_menu' => 10,
                'id_sub_menu' => 10
            ]
        );

        $response
            ->assertStatus(422)
            ->assertJson(['created' => false]);
    }

    public function testShouldInsertPrintMenu()
    {
        $response = $this->json(
            'POST',
            '/api/print_menus',
            [
                'id_menu' => 0,
                'id_sub_menu' => 9
            ]
        );

        $response
            ->assertStatus(201)
            ->assertJson(['created' => true]);
    }

    public function testShouldNotUpdateForStringId()
    {
        $response = $this->json(
            'PUT',
            '/api/print_menus/string_id',
            [
                'id_menu' => 1,
                'id_sub_menu' => 4
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotUpdateForChildMenuIsEqualToParentId()
    {
        $response = $this->json(
            'PUT',
            '/api/print_menus/4',
            [
                'id_menu' => 4,
                'id_sub_menu' => 4
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotUpdateForPrintMenuNotExists()
    {
        $response = $this->json(
            'PUT',
            '/api/print_menus/12312312',
            [
                'id_menu' => 1,
                'id_sub_menu' => 4
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldUpdatePrintMenu()
    {
        $response = $this->delete('/api/print_menus/4');

        $response->assertStatus(200);
    }

    public function testShouldNotDestroyForStringId()
    {
        $response = $this->delete('/api/print_menus/string_id');

        $response->assertStatus(422);
    }

    public function testShouldDestroyPrintMenu()
    {
        $response = $this->delete('/api/print_menus/4');

        $response->assertStatus(200);
    }
}
