<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Menu;
use Illuminate\Support\Facades\Bus;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MenuTest extends TestCase
{
    // use RefreshDatabase;
    use WithFaker;
    
    public function testShouldInsertMenu() 
    {
        $response = $this->json(
            'POST', 
            '/api/menus', 
            [
                'name' => 'Menu ' . $this->faker->name,
                'icon' => $this->faker->title,
                'uri' => $this->faker->url,
                'state' => 1
            ]
        );

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true
            ]);
    }

        
    public function testShouldNotInsertMenuForBadParameters() 
    {
        $response = $this->json(
            'POST', 
            '/api/menus', 
            [
                'name' => '',
                'icon' => $this->faker->title,
                'uri' => $this->faker->url,
                'state' => 100
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotInsertMenuForBadMethod()
    {
        $response = $this->json(
            'PUT', 
            '/api/menus', 
            [
                'name' => 'Menu ' . $this->faker->name,
                'icon' => $this->faker->title,
                'uri' => $this->faker->url,
                'state' => 1
            ]
        );

        $response->assertStatus(405);
    }

    public function testShouldUpdateMenu() 
    {
        $response = $this->json(
            'PUT', 
            '/api/menus/1', 
            [
                'name' => 'Menu ' . $this->faker->name,
                'icon' => $this->faker->title,
                'uri' => $this->faker->url,
                'state' => 1
            ]
        );

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true
            ]);
    }

    public function testShouldNotUpdateMenuForBadParameters()
    {
        $response = $this->json(
            'PUT', 
            '/api/menus/1', 
            [
                'name' => '',
                'icon' => $this->faker->title,
                'uri' => $this->faker->url,
                'state' => 0
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotUpdateMenuForStringId()
    {
        $response = $this->json(
            'PUT', 
            '/api/menus/string_id', 
            [
                'name' => 'Menu ' . $this->faker->name,
                'icon' => $this->faker->title,
                'uri' => $this->faker->url,
                'state' => 1
            ]
        );

        $response
            ->assertStatus(422)
            ->assertJson([
                'updated' => false
            ]);
    }

    public function testShouldNotUpdateMenuForMenuNotExists()
    {
        $response = $this->json(
            'PUT', 
            '/api/menus/1231231412', 
            [
                'name' => 'Menu ' . $this->faker->name,
                'icon' => $this->faker->title,
                'uri' => $this->faker->url,
                'state' => 1
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldDeleteMenu()
    {
        $response = $this->delete('/api/menus/1');

        $response
            ->assertStatus(200)
            ->assertJson(['deleted' => true]);
    }

    public function testShouldNotDeleteMenuForStringId()
    {
        $response = $this->delete('/api/menus/string_id');

        $response
            ->assertStatus(422)
            ->assertJson(['deleted' => false]);
    }

    public function testShouldNotDeleteMenuForMenuNotExists()
    {
        $response = $this->delete('/api/menus/112421412');

        $response->assertStatus(204);
    }

    public function testShouldGetMenus()
    {
        $response = $this->get('/api/menus/');

        $response->assertStatus(200);
    }

    public function testShouldGetSingleMenu()
    {
        $response = $this->get('/api/menus/2');

        $response->assertStatus(200);
    }

    public function testShouldNotGetSingleMenuForStringId()
    {
        $response = $this->get('/api/menus/string_id');

        $response->assertStatus(422);
    }
}
