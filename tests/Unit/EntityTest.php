<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Freshwork\ChileanBundle\Rut;

class EntityTest extends TestCase
{
    use WithFaker;
    
    public function testShouldInsertEntity() 
    {
        $random_number = rand(1000000, 25000000);
        $rut = new Rut($random_number);

        $response = $this->json(
            'POST', 
            '/api/entities', 
            [
                'name' => 'Entity ' . $this->faker->name,
                'state' => 1,
                'razon_social' => $this->faker->company,
                'rut' => $rut->fix()->format(),
                'url_img' => $this->faker->url
            ]
        );

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true
            ]);
    }

        
    public function testShouldNotInsertEntityForBadParameters() 
    {
        $random_number = rand(1000000, 25000000);
        $rut = new Rut($random_number);

        $response = $this->json(
            'POST', 
            '/api/entities', 
            [
                'name' => '',
                'state' => 100,
                'razon_social' => $this->faker->company,
                'rut' => $rut->fix()->format(),
                'url_img' => $this->faker->url
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotInsertEntityForBadMethod()
    {
        $random_number = rand(1000000, 25000000);
        $rut = new Rut($random_number);
        
        $response = $this->json(
            'PUT', 
            '/api/entities', 
            [
                'name' => 'Entity ' . $this->faker->name,
                'state' => 1,
                'razon_social' => $this->faker->company,
                'rut' => $rut->fix()->format(),
                'url_img' => $this->faker->url
            ]
        );

        $response->assertStatus(405);
    }

    public function testShouldUpdateEntity() 
    {
        $random_number = rand(1000000, 25000000);
        $rut = new Rut($random_number);

        $response = $this->json(
            'PUT', 
            '/api/entities/1', 
            [
                'name' => 'Entity ' . $this->faker->name,
                'state' => 1,
                'razon_social' => $this->faker->company,
                'rut' => $rut->fix()->format(),
                'url_img' => $this->faker->url
            ]
        );

        $response
            ->assertStatus(200)
            ->assertJson([
                'updated' => true
            ]);
    }

    public function testShouldNotUpdateEntityForBadParameters()
    {
        $random_number = rand(1000000, 25000000);
        $rut = new Rut($random_number);

        $response = $this->json(
            'PUT', 
            '/api/entities/1', 
            [
                'name' => '',
                'state' => 1,
                'razon_social' => $this->faker->company,
                'rut' => $rut->fix()->format(),
                'url_img' => $this->faker->url
            ]
        );

        $response->assertStatus(422);
    }

    public function testShouldNotUpdateEntityForStringId()
    {
        $random_number = rand(1000000, 25000000);
        $rut = new Rut($random_number);

        $response = $this->json(
            'PUT', 
            '/api/entities/string_id', 
            [
                'name' => 'Entity ' . $this->faker->name,
                'state' => 1,
                'razon_social' => $this->faker->company,
                'rut' => $rut->fix()->format(),
                'url_img' => $this->faker->url
            ]
        );

        $response
            ->assertStatus(422)
            ->assertJson([
                'updated' => false
            ]);
    }

    public function testShouldNotUpdateEntityForEntityNotExists()
    {
        $random_number = rand(1000000, 25000000);
        $rut = new Rut($random_number);
        
        $response = $this->json(
            'PUT', 
            '/api/entities/1231231412', 
            [
                'name' => 'Entity ' . $this->faker->name,
                'state' => 1,
                'razon_social' => $this->faker->company,
                'rut' => $rut->fix()->format(),
                'url_img' => $this->faker->url
            ]
        );

        $response->assertStatus(204);
    }

    public function testShouldDeleteEntity()
    {
        $response = $this->delete('/api/entities/1');

        $response
            ->assertStatus(200)
            ->assertJson(['deleted' => true]);
    }

    public function testShouldNotDeleteEntityForStringId()
    {
        $response = $this->delete('/api/entities/string_id');

        $response
            ->assertStatus(422)
            ->assertJson(['deleted' => false]);
    }

    public function testShouldNotDeleteEntityForEntityNotExists()
    {
        $response = $this->delete('/api/entities/112421412');

        $response->assertStatus(204);
    }

    public function testShouldGetEntity()
    {
        $response = $this->get('/api/entities/');

        $response->assertStatus(200);
    }

    public function testShouldGetSingleEntity()
    {
        $response = $this->get('/api/entities/2');

        $response->assertStatus(200);
    }

    public function testShouldNotGetSingleEntityForStringId()
    {
        $response = $this->get('/api/entities/string_id');

        $response->assertStatus(422);
    }
}
